#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PyQt5 import QtGui, QtWidgets, QtCore 
import os, sys
import time, threading, logging, logging.handlers
import gui_ui, reaper_logic, compare_logic
import helpers as h
import _table_view

_Start = 'Start'
_Stop  = 'Stop'

class Reap_Window(QtWidgets.QMainWindow, gui_ui.Ui_Reaper):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.reaper = reaper_logic.Reaper_logic()

        self.ok_button = self.buttonBox.button(QtWidgets.QDialogButtonBox.Ok)
        self.ok_button.setText(_Start)

        self.from_btn.clicked.connect(self.set_from_path)
        self.to_btn.clicked.connect(self.set_into_path)
        self.buttonBox.accepted.connect(self.start_stop)
        self.buttonBox.rejected.connect(self.close)
        
        # Compare tab
        self.from_btn_cmp.clicked.connect(self.set_cmp_from_path)
        self.to_btn_cmp.clicked.connect(self.set_cmp_with_path)
        self.buttonBox_cmp.accepted.connect(self.compare)
        self.buttonBox_cmp.rejected.connect(self.close)
        self.check_sub_cmp.stateChanged.connect(self.compare)

        # LOGGING set-up
        log_format = '%(asctime)s:%(msecs)03d   %(name)-8s %(levelname)-8s %(message)s'
        date_fmt = '%Y-%m-%d %H:%M:%S'
        logging.basicConfig(format=log_format, level=logging.DEBUG, datefmt=date_fmt)
        log_formatter = logging.Formatter(fmt=log_format, datefmt=date_fmt)
        main_log = logging.FileHandler(h.log_file_path())
        main_log.setLevel(logging.INFO)
        main_log.setFormatter(log_formatter)
        # Back-up log: backed up one a week and max 5 are kept
        back_up_log = logging.handlers.TimedRotatingFileHandler(filename=h.logs_location()+'_', 
                                                                 when='w0', backupCount=5)
        back_up_log.suffix = back_up_log.suffix + '.log'
        back_up_log.setLevel(logging.INFO)
        back_up_log.setFormatter(log_formatter)

        logging.getLogger('').addHandler(back_up_log)
        logging.getLogger('').addHandler(main_log)
        self.gui_log = logging.getLogger('GUI')
        self.thread_log = logging.getLogger('REAPER')
        
        logging.info('Back-up log is set.')
        logging.info('Main log is set.')
        logging.debug('T count: ' + str(threading.activeCount()))
        

    def exit_reaper(self):
        """ Ensures that the thread and loggin are closed down properly.
        """
        self.reaper.exit.set()
        logging.debug('Closing ... T count: ' + str(threading.activeCount()))
        logging.shutdown()

    def closeEvent(self, event):
        self.exit_reaper()
        event.accept()

    def set_from_path(self):
        from_p = QtWidgets.QFileDialog.getExistingDirectory(self, 'Select the directory you want to copy files from')
        the_path = QtCore.QDir.toNativeSeparators(str(from_p))
        self.from_path.setText(the_path)
        self.reaper.from_dir = the_path
        self.gui_log.info('From directory set to: ' + the_path)

    def set_into_path(self):
        to_p = QtWidgets.QFileDialog.getExistingDirectory(self, 'Select the directory you want to copy files into')
        the_path = QtCore.QDir.toNativeSeparators(str(to_p))
        self.to_path.setText(the_path)
        self.reaper.to_dir = the_path
        self.gui_log.info('To directory set to: ' + the_path)

    def update_reaper(self):
        if (self.from_path.text() == '') or (self.to_path.text() == '') :
            QtWidgets.QMessageBox.warning(self, "Error",
                            'Please select both a \'From\' and a \'To\' directory.')
            raise ValueError
        elif self.from_path.text() == self.to_path.text():
            QtWidgets.QMessageBox.warning(self, "Error",
                            'Cannot copy files into the same directory. \n' +
                            'Please select different \'From\' and \'To\' directories.')
            raise ValueError
        else: 
            self.reaper.from_dir = self.from_path.text()
            self.reaper.to_dir = self.to_path.text()
        
        self.reaper.folder_prefix = self.folder_pre.text().strip()
        self.reaper.file_types = self.get_file_types()
        self.reaper.poll_freq = self.poll_freq.value()
        self.reaper.within_days = self.monit_days.value()

    def start_stop(self):
        logging.debug('T count: ' + str(threading.activeCount()))
        try:
            self.update_reaper()
            if self.ok_button.text() == _Start:
                self.start_polling()
                self.ok_button.setText(_Stop)
                now_str = time.strftime(" %H:%M:%S %d-%m-%Y", time.localtime())
                self.statusbar.showMessage('Running since ' + now_str)
            else:
                self.ok_button.setText(_Start)
                self.reaper.exit.set()
                self.statusbar.clearMessage()
                self.gui_log.info('Stopped in GUI.')
                logging.debug(str(self.poll_thread.is_alive()) + ': ' + str(threading.activeCount()))
        except ValueError as v:
            self.gui_log.error('Directories not set, or set uncorrectly. {}'.format(str(v)))
        except Exception as e:
            self.reaper.exit.set()
            self.gui_log.error('Something went wrong when trying to start reaping.' + '\n' +
                                '\t\tError: ' + str(e))
           
    def start_polling(self):
        self.reaper.exit.clear()
        self.poll_thread = threading.Thread(target=self.reaper.scan_from_dir)
        self.start_info()
        self.poll_thread.start()
        logging.debug(str(self.poll_thread.is_alive()) + ': ' + str(threading.activeCount()))

    def start_info(self):
        self.gui_log.info('Starting. \n' +
                    '\t\tFrom dir: ' + self.reaper.from_dir + '\n' + 
                    '\t\tTo dir: ' + self.reaper.to_dir + '\n' +
                    '\t\tDir prefix: ' + self.reaper.folder_prefix + '\n' +
                    '\t\tInterval: ' + str(self.reaper.poll_freq) + ' minute(s).\n'
                    '\t\tTypes: ' + str(self.reaper.file_types) + '\n')

    def get_file_types(self):
        ftypes = self.file_type.text().split(',')
        file_types = []

        for a_type in ftypes:
            if a_type not in [' ', '']: 
                file_types.append(a_type.strip().lower())
        return file_types

    # Compare tab functions - _table_view.py file.
    def set_cmp_from_path(self):
        _table_view.set_cmp_from_path(self)
        #self.compare()

    def set_cmp_with_path(self):
        _table_view.set_cmp_with_path(self)
        #self.compare()

    def compare(self):
        _table_view.compare_and_populate(self)
    

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    the_reaper = Reap_Window()
    the_reaper.show()
    sys.exit(app.exec_())