#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import filehash as fs
import datetime as dt



def today_str():
    return str(dt.date.today())


def path_concat(path, second_part):
    '''  Concatenates second part onto the given path, whether it's 
        a file or a directory. Checks the OS and concatenates accordingly.
        :parameter path: string
        :parameter second_part: string
        @rtype: string
    '''
    return path + os.sep + second_part


def path_xconcat(path, parts=[]):
    """ Concatenates everything in the given list onto the path with the OS appropriate
        separator, in the given order. 
        @rtype: string
    """
    tmp_path = path
    for p in parts: 
        tmp_path = path_concat(tmp_path, p)
    return tmp_path


def create_dir(dir_path):
    """ Checks if directory exists and creates it if not.
        :parameter dir_path: Full directory path
    """
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)


def log_file_name():
    return today_str() + '.log'
     

def logs_location():
    """ Creates a separate directory for logs if it doesn't already exist.
        @return: full path of where log files will be -> current path\\reaper_logs
        @rtype: str
    """
    log_path = os.path.realpath('.') + os.sep + 'reaper_logs'
    create_dir(log_path)
    file_path = log_path  + os.sep
    return file_path


def log_file_path():
    """ Creates the log directory if doesn't exist and sets the log file name
        in this directory.
        @return: full path to log file
        @rtype: string
    """
    return logs_location()  + log_file_name()


def get_suffix(file_name):
    """ @return: suffix of the file, i.e. file type
        @rtype: string
    """
    tmp = file_name.split('.')
    return tmp[-1].lower()


def get_hash(directory, file_name):
    """ @return: sha256 has for file in the given directory
        @rtype: string
    """
    the_hash = ''
    sha256 = fs.FileHash('sha256')
    try:
        the_hash = sha256.hash_file(path_concat(directory, file_name))
    except:
        pass
    return the_hash




