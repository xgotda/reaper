#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, logging

_name = 'name'
_dir  = 'dir'
_size = 'size'
_doC  = 'dir_of_copy'
_soC  = 'size_of_copy'
_comment = 'comment'

_Duplicate = 'Duplicate. '
_Size_diff = "Size mismatch. "

def compare(from_, with_):
    """ Compare logic. Returns stuff to populate table. """

    """ 
    read to dir and subdirs, put in dict

    loop through res = dir_content_list
    see if exitst in dict (name should be exactly the same)
    add full details to res - extend array with [dir_of_copy] and [size_of_copy] next to [file] and [size]
    make sure to enter empty alternatives if file isn't found so that the array's details match (dict instead?)
    return super full array and no of files to be shown in table
    """

    #See test.py
    org_from = from_.copy()
    for dK, dir_vals in from_.items():
        for eK, eV in dir_vals.items():
            if not eK == _name:
                if eK in with_.keys():
                    eV[_doC] = with_[eK].get(_dir)
                    eV[_soC] = with_[eK].get(_soC)
                    if eV[_size] != eV[_soC]:
                        eV[_comment] = f"{_Size_diff} {eV[_comment]}"
                        print(f"Comment: {eV[_comment]}")

    return from_

"""#See test.py
org_from = from_.copy()
for dK, dir_vals in from_.items():
    print(dK)
    print(dir_vals)
    print('--------')
    for eK, eV in dir_vals.items():
        #print('>>>> ' + eK)
        if not eK == _name:
            if eK in with_.keys():
            # WHEN building wud dir, if file exists in two places put in special dict (in wud) 
            # and deal with at the end (need to show duplicates in table.)
                print('********' + eK +' ' + with_[eK].get(_dir))
                eV[_doC] = with_[eK].get(_dir)
                eV[_soC] = with_[eK].get(_soC)
                print(eV)
                print('************')
return from_"""

def into_files(a_path):
    """ Read the directory and it's subdirectories and save to a dictionary.
        Returns a list of dictionaries, one for the full dir and one for duplicates. (deal with later!)
        @return: List containing two dictionaries, full list and duplicates
        @rtype: list
    """
    all_f = {}
    dup_f = {}
    for root, dirs, files in os.walk(a_path):
        curr_dir = os.path.relpath(root, a_path)
        for name in files:
            a_file = { _name : name, 
                      _soC : get_size(root, name),
                      _dir : curr_dir}
            if name in all_f:
                a_file[_name] = ''.join([a_file[_name],'_II'])
                #logging.debug(f"Duplicate INTO!!! {name} --- in {a_path}.")
                dup_f.setdefault(a_file[_name], a_file)
            all_f.setdefault(a_file[_name], a_file)
    
    logging.debug(f"Number of duplicates: {len(dup_f)}.")
    return [all_f, dup_f]

def dir_content_dict(dir_path, inc_subs):
    """ Create a dictionary of files in dir_path
    """
    a_dir = {}
    full_list = {}
    no_of_files = 0
    for root, dirs, files in os.walk(dir_path):
        no_of_files += len(files)
        curr_dir = os.path.relpath(root, dir_path)
        a_dir.setdefault(_name, curr_dir)
        for name in files:
            a_file =  { _dir  : curr_dir,
                        _name : name, 
                        _size : get_size(root, name),
                        _soC : -1,
                        _doC  : '',
                        _comment : ''}
            if name in a_dir:
                name = ''.join([a_file[_name],'_duplicate'])
                a_file[_comment] = _Duplicate
            a_dir.setdefault(name, a_file)
        full_list.setdefault(a_dir[_name], a_dir)        
        if not inc_subs:
            break
    return full_list, no_of_files

def get_size(root, file_name):
    size = 0
    path = os.path.join(root, file_name)
    try:
        size = os.path.getsize(path)
    except OSError as err:
        try: 
            logging.error(f"The file is either inaccessible or doesn't exist. The following error occured: '{err}'. ")
        except UnicodeEncodeError as uerr: 
            print("UNICODE ERR")
            logging.error(f"The file is either inaccessible or doesn't exist. The following error occured: '{uerr}'. ")
    finally:
        return size

def delete_item():
    """ Deletes selected item (in 'from' dir only).
        Separate function to delete more than one at a time?
        (Several selected) """
    pass


"""def name_size_list(dir_path):
    from_list = []
    size_list = []
    with os.scandir(dir_path) as it:
        for item in it:
            if not item.name.startswith('.') and item.is_file():
                from_list.append(item.name)
                size_list.append(item.stat().st_size)
                print(item.name + ' \t' + str(item.stat().st_size))
    #print('\n')
    full_list = [from_list, size_list]
    return full_list"""