#!/usr/bin/env python3

import os, sys, re, logging, time
import datetime as dt
import shutil
from threading import Event
import helpers as hs

_dt_format = '%y%m%d'
_dt_cformat = '%Y%m%d'  #Year with century
logger = logging.getLogger('REAPER')
_size1 = 'size1'
_size2 = 'size2'
_fcp = 'full_copy_path'

class Reaper_logic(object):
    def __init__(self):

        self.from_dir = '.'
        self.to_dir = '.'
        self.folder_prefix = ''
        self.file_types = []
        self.poll_freq = 1
        self.within_days = 3
        self.current_files = {}
        self.exit = Event()


    @property
    def poll_freq(self):
        """ Returns the poll frequency in minutes \n
            @rtype: int
        """
        return int(self._poll_freq / 60)


    @poll_freq.setter
    def poll_freq(self, minutes):
        """ Sets the internal poll value to seconds.
        """
        self._poll_freq = minutes * 60


    def scan_from_dir(self):
        self.current_files = {}
        while not self.exit.is_set():
            with os.scandir(self.from_dir) as it:
                for entry in it:
                    entry_name = entry.name
                    if not entry_name.startswith('.') and entry.is_file():
                        if self.current_files.get(entry_name, False):
                            if self.size_check(entry):
                                self.move(entry)
                            else:
                                logger.debug('----- Different size ---- \t' + entry_name + ' \t'
                                        + str(self.current_files.get(entry_name)))
                        else:
                             self.to_current(entry)

            logger.debug('Scanned')
            logger.debug(self.current_files)
            self.exit.wait(self._poll_freq)
        logger.info('Stopping scanning')
        # Emit signal to GUI?


    def to_current(self, entry):
        """ Adds the entry into the current_files list if it's of the correct type,
            within the date range we are checking (default three days back),
            and not already copied.
        """
        entry_name = entry.name
        if self.correct_type(entry_name) and self.valid_date(entry_name):
            if not self.copied(entry_name):
                self.current_files[entry_name] = {_size1: entry.stat().st_size, _size2: 0}


    def valid_date(self, file_name):
        """ Checks if the file isn't older than what's stated by within_days.
            @return: False if the file is older, i.e. not valid.
            @rtype: boolean
        """
        valid = False
        date = self.file_date(file_name)
        if date != 0:
            today = dt.datetime.today()
            try:
                delta = today - date
                days_diff = delta.days
                if days_diff <= self.within_days:
                    valid = True
            except Exception as err:
                logger.debug('error calculating date delta.' + str(err))
        return valid


    def file_date(self, file_name):
        """ Extracts the date from the file name and converts to datetime. Returns 0 if no date found.
            @rtype: datetime
        """
        the_date = 0
        try:
            found = re.search(r'\d{8}', file_name)
            if found:
                the_date = dt.datetime.strptime(found[0], _dt_cformat)
            else:
                found = re.search(r'\d{6}', file_name)
                if found:
                    the_date = dt.datetime.strptime(found[0], _dt_format)
        except Exception as err:
            logger.error('Error for file ' + file_name + ' when trying to convert '
                        + str(found[0]) + ' to datetime format. \n' + str(err))
        finally:
            return the_date


    def date_str(self, file_name):
        """ Extracts the date from the file name.
            Expected date format in file name: YYMMDD, which is surrounded by '_'.
            @return: Date in file name, zero otherwise.
            @rtype: string
        """
        try:
            the_date = dt.datetime.strftime(self.file_date(file_name), _dt_format)
        except:
            the_date = '0'
        return the_date


    def size_check(self, entry):
        """ Checks if the size of the file has changed since last checked and the time before that.
            Updates internal list with new size if changed.\n
            @return: True if file size hasn't changed, False otherwise. \n
            @rtype: boolean
        """
        name = entry.name
        new_size = entry.stat().st_size
        size1 = self.current_files[name][_size1]
        size2 = self.current_files[name][_size2]
        unchanged = False

        if  size2 == 0:
            self.current_files[name][_size2] = new_size
        elif (size1 == size2 == new_size):
            unchanged = True                #copy
        else:
            self.current_files[name][_size1] = size2
            self.current_files[name][_size2] = new_size

        return unchanged


    def move(self, entry):
        """ Copy file if it's no longer growing. Extract_dir_name, and create dir
         if doesn't already exist.
        """
        name = entry.name
        # copy file (overwrite if exists? append _1 if not?)
        to_p = self.full_copy_path(name)
        hs.create_dir(to_p)
        copied_into = shutil.copy2(hs.path_concat(self.from_dir, name), to_p)

        logging.debug(name + '\t SAME SIZE: ' + str(self.current_files[name]))
        logger.info('Copying file: ' + name + '\t\tFrom dir: '
                    + self.from_dir + '\t\tTo dir: ' + to_p)

        # Hashes
        pre_copy_hash = hs.get_hash(self.from_dir, name)
        post_copy_hash = hs.get_hash(to_p, name)
        if pre_copy_hash == post_copy_hash:
            logger.info('Verified copied file:\t' + copied_into)
            # logger.info('Veryfied copied file:\t' + copied_into + ':\t MATCH!')
            try:
                del(self.current_files[name])
            except Exception as e:
                logger.warning('Cannot delete entry ' + name + '.' + str(e) )
        else:
            #TODO: error handler! Remove copied file and wait for next cycle?
            logger.warning('Something went WRONG when copying...' + name)


    def correct_type(self, file_name):
        """ Checks if this file is of the type to be copied
            @returns: True if correct type, False otherwise
            @rtype: boolean
        """
        correct = False
        if self.file_types == []:
            correct = True
        else:
            f_type = hs.get_suffix(file_name)
            correct = f_type in self.file_types
        return correct


    def copied(self, file_name):
        """ Checks if the file already exists in the correct directory.
            @rtype: boolean
        """
        file_path = hs.path_concat(self.full_copy_path(file_name), file_name)
        logger.debug('ALREADY COPIED?  -- ' + file_path +' : ' + str(os.path.exists(file_path)).upper())
        return os.path.exists(file_path)


    def extract_dir_name(self, file_name):
        """ Extracts the directory date in the format YYMM from the file name
            and appends it to the folder prefix.\n
            @return: directory prefix, underscore, YYMM of date_st \n
            @rtype: string
        """
        return self.folder_prefix + '_' + self.date_str(file_name)[0:4]


    def full_copy_path(self, file_name):
        ''' Returns full path of directory for the file to be copied into.
        '''
        return hs.path_concat(self.to_dir, self.extract_dir_name(file_name))
