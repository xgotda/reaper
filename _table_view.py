#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PyQt5 import QtGui, QtWidgets, QtCore
import compare_logic as cl

# self is Reaper window

_dir = 0
_file = 1
_size = 2
_headers = ['dir', 'file', 'size', 'copy size', 'copy dir', 'comment']

def compare_and_populate(self):
    [table_content, no_of_files] = get_comparisons(self)
    set_up_table(self, table_content, no_of_files)


def get_comparisons(self):
    """ Get content of 'from' dir and of 'to' dir. Compare 'from' and 'to' and create one list.
    """
    subs = self.check_sub_cmp.isChecked()
    [full_dir_dict, no_of_files] = cl.dir_content_dict(self.from_path_cmp.text(), subs)
    [all_files_dict, duplicates_dict] =  cl.into_files(self.to_path_cmp.text()) 
    full_table = cl.compare(full_dir_dict, all_files_dict)

    return [full_table, no_of_files]

def set_up_table(self, table_items, rows):
    """ Sets up the table rows and columns.
        Populates the table with passed in items.
    """
    self.tableWidget.setColumnCount(len(_headers))
    self.tableWidget.setRowCount(rows)
    self.tableWidget.setHorizontalHeaderLabels(_headers)
    self.tableWidget.setSortingEnabled(False)

    populate_table(self, table_items)

    self.tableWidget.resizeColumnsToContents()
    self.tableWidget.horizontalHeader().setStretchLastSection(True)
    self.tableWidget.setSortingEnabled(True)

def populate_table(self, table_items):
    """ Populate table from dictionary. """
    row = 0
    for dK, dir_val in table_items.items():
        for file_key, file_val in dir_val.items():
            col = 0
            if not file_key == cl._name: 
                for val in file_val.values():
                    self.tableWidget.setItem(row, col, QtWidgets.QTableWidgetItem(str(val)))
                    self.tableWidget.item(row, col).setBackground(get_colour(file_val))
                    col += 1
                row += 1

def get_colour(file_values):
    """ Gets the appropriate colour based on error rules pertaining to size.
    """
    org_size = file_values[cl._size]
    copy_size = file_values[cl._soC]
    colour = QtGui.QColor(254, 254, 254)

    if copy_size == -1:
        colour = QtGui.QColor(236, 112, 99) #not found = red
    elif org_size != copy_size:
        colour = QtGui.QColor(255, 205, 210) #different size = pink
    elif copy_size == 0 :
        colour = QtGui.QColor(176, 190, 197) #file error = grey

    return colour
    
def set_cmp_from_path(self):
    from_p = QtWidgets.QFileDialog.getExistingDirectory(self, 'Select the directory you want to compare files from')
    the_path = QtCore.QDir.toNativeSeparators(str(from_p))
    self.from_path_cmp.setText(the_path)
    self.gui_log.info('Compare from directory set to: ' + the_path)

def set_cmp_with_path(self):
    to_p = QtWidgets.QFileDialog.getExistingDirectory(self, 'Select the directory you want to compare with')
    the_path = QtCore.QDir.toNativeSeparators(str(to_p))
    self.to_path_cmp.setText(the_path)
    self.gui_log.info('Compare with directory set to: ' + the_path)
