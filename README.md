# Reaper

- Poll frequency - how often the programs checks for changes, in minutes. (Will change this once we are sure there are no major issues.)
- Monitor days - how far back (in time) the Reaper checks for new and/or updated files.
- Verification - hash calculated using sha256.
- Copying keeps metadata.
- Size check - performed 3 times before copying.
- Logs - in subdirectory "reaper_logs" of where the Reaper.exe is
        - main log is named YY-MM-DD.log
        - Logs are backed up every Monday (providing that the Reaper is running). Named: _.YY-MM-DD.log


# Build details
Environment in reaper_env.txt
use _main.spec with pyinstaller - literally run:
pyinstaller _main.spec


# Git
https://bitbucket.org/xgotda/reaper/src/master/